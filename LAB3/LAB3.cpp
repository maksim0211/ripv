﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define max_num 300000
int a[max_num], sorted_omp[max_num];//, sorted_ser[max_num];
int n;
int numthreads;
void openmp_alg(int numthreads);
void serial_alg();

int main() {
        while (true) {
            printf("Set count array's elements(1 < N < 300000, enter 0 to end)\n");
            scanf_s("%d", &n);
            printf("Set num_threads\n");
            scanf_s("%d", &numthreads);
            if (n >= max_num) {
                puts("Count great or equal than 300000, set again");
                continue;
            }
            else if (n < 0) {
                puts("Negative number, set again");
                continue;
            }
            else if (n == 0) {
                puts("App is closing...");
                return 404;
            }
            for (int i = 0; i < n; i++) {
                a[i] = rand() % 4 + 1;
                sorted_omp[i] = 0;
                //sorted_ser[i] = 0;
            }
            serial_alg();
            for (int i = 0; i < n; i++) {
                sorted_omp[i] = 0;
                //sorted_ser[i] = 0;
            }
            openmp_alg(numthreads);

        }
    return 0;
}

void openmp_alg(int numthreads) {
    double start_time = omp_get_wtime();
    omp_set_num_threads(numthreads);
#pragma omp parallel num_threads(numthreads)
    {
#pragma omp for
        for (int i = 0; i < n; i++) {
            int cnt = 0;
            for (int j = 0; j < n; j++) {
                if (a[i] > a[j])
                    cnt++;
            }
            while (sorted_omp[cnt] != 0)
                cnt++;
            sorted_omp[cnt] = a[i];
        }
    }
    double end_time = omp_get_wtime();
    printf("Parallel algorithm time: %f s\n", end_time - start_time);
}

void serial_alg() {
    int i, j, count;
    double start_time = omp_get_wtime();
    for (i = 0; i < n; i++) {
        count = 0;
        for (j = 0; j < n; j++) {
            if (a[i] > a[j]) {
                count++;
            }
        }
        while (sorted_omp[count] != 0)
            count++;
        sorted_omp[count] = a[i];
    }
    double end_time = omp_get_wtime();
    printf("\nSerial algorithm time: %f s\n", end_time - start_time);
}
