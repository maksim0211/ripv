package LAB1;

import scala.Int;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Lab1Parallel {
    static int diap = 1_0;
    static int ARR_SIZE = (int) Math.pow(2, 29);
    static int min = Integer.MAX_VALUE;
    static int max = Integer.MIN_VALUE;
    final static int[] arr1 = new int[ARR_SIZE];;
    final static int[] cntIndexOffset = new int[diap];
    final static int[] cnt = new int[diap];
    void fillStartData() {
        Random rnd = new Random();
        for (int i = 0; i < ARR_SIZE; i++) {
            arr1[i] = rnd.nextInt(diap) + 1;
        }
        for (int i = 0; i < diap; i++)
            cnt[i] = 0;
    }

    public void sort(String[] args){
        fillStartData();
        int n = Integer.parseInt(args[0]);
        System.out.println("Count of threads: " + n);
        class MyThread extends Thread{
            int l;
            int r;
            public final int[] mycnt = new int[diap];
            public MyThread(int l, int r){
                this.l = l;
                this.r = r;
            }

            @Override
            public void run(){
                 for (int i = l; i < r; i++) {
                    mycnt[arr1[i] - min]++;
                }
                for (int i = 0; i < diap; i++) {
                    synchronized (cnt) {
                        cnt[i] += mycnt[i];
                    }
                }
            }
        }

        class MyThread2 extends Thread {
            int start;
            public MyThread2(int start) {
                this.start = start;
            }

            @Override
            public void run() {
                fillArray(start);
            }

            public void fillArray(int start){
                for(int i = start; i < max; i = i + n)
                    Arrays.fill(arr1,cntIndexOffset[i],cntIndexOffset[i] + cnt[i],i+min);
            }
        }

        List<MyThread> list = new ArrayList<>(n);
        List<MyThread2> list2 = new ArrayList<>(n);
        for(int i = 0; i < n; i++) {
            list2.add(new MyThread2(i));
            if (i + 1 != n)
                list.add(new MyThread(arr1.length / n * i, arr1.length / n * (i + 1)));
            else
                //Чтобы не "потерять" последние элементы, если число элементов не делится нацело на число потоков
                list.add(new MyThread(arr1.length / n * i, arr1.length));
        }

        long t1 = System.currentTimeMillis();
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] < min) {
                min = arr1[i];
            }
            else if (arr1[i] > max) {
                max = arr1[i];
            }
        }
        long t11 = System.currentTimeMillis();
        System.out.println("Time finding min and max:" + (t11 - t1));

        long t3 = System.currentTimeMillis();
        list.forEach(x -> x.start());
        list.forEach(x ->{
            try {
                x.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


        System.out.println("Time of work multithread: " + (System.currentTimeMillis() - t3));
        long t5 = System.currentTimeMillis();

        int tmpOffset = 0;
        for(int i = 0; i < diap; i++) {
            cntIndexOffset[i] = tmpOffset;
            tmpOffset += cnt[i];
        }
        list2.forEach(x -> x.start());
        list2.forEach(x -> {
            try {
                x.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        long t2 = System.currentTimeMillis();
        System.out.println("Time fill arr: " + (t2-t5) + "ms \t");
        System.out.println("Time parallel algo: " + (t2-t1) + "ms");
        System.out.println("Time parallel algo without findMinMax: " + ((t2-t1) - (t11 - t1)) + "ms \n\n");
    }
}
