package LAB1;

public class MainLab1 {
    public static void main(String[] args) {
        int cnt = 6;
        int n = 1;
        while (cnt != 0) {
            if(1 == 1) {
                args[0] = String.valueOf(n);
                Lab1Parallel l1 = new Lab1Parallel();
                l1.sort(args);
                n++;
            }
            else {
                Lab1Serial l1 = new Lab1Serial();
                System.out.println(l1.serialSort());
            }
            System.out.println("---------------------------");
            cnt--;
        }
    }
}
