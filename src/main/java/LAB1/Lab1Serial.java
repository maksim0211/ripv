package LAB1;

import java.util.Random;

public class Lab1Serial {

    private int DIAP;
    int[] arr1;
    int[] count;

    void fillArray() {
        DIAP = 1_000_000;
        int ARR_SIZE = (int) Math.pow(2, 29);
        arr1 = new int[ARR_SIZE];
        count = new int[DIAP];
        Random rnd = new Random();
        for (int i = 0; i < ARR_SIZE; i++) {
            arr1[i] = rnd.nextInt(DIAP) + 1;
        }
    }

    public String serialSort(){
        this.fillArray();
        long t1 = System.currentTimeMillis();
        sortAlgSerial(this.arr1);
        long t2 = System.currentTimeMillis();
        return "Time: " + (t2-t1) + "ms \n\n";
    }

    public int[] sortAlgSerial(int[] array) {
        int min, max;
        max = min = array[0];
        long t11 = System.currentTimeMillis();
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
            else if (array[i] > max) {
                max = array[i];
            }
        }
        long t111 = System.currentTimeMillis();
        System.out.println("Time finding min max:" + (t111 - t11));
        return this.sortAlgSerial(array, min, max);
    }

    int[] sortAlgSerial(int[] array, int min, int max) {
        for (int i = 0; i < array.length; i++) {
            count[array[i] - min]++;
        }
        int idx = 0;
        for (int i = 0; i < count.length; i++) {
            for (int j = 0; j < count[i]; j++) {
                array[idx++] = i + min;
            }
        }
        return array;
    }
}
