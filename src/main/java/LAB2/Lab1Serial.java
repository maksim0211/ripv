package LAB2;

import java.util.Random;

public class Lab1Serial {

    public static void main(String[] args) {
        System.out.println("Serial :" + new Lab1Serial().serialSort());
    }

    private int DIAP;
    int[] arr1;
    int[] count;
    void fillArray() {
        DIAP = 2;
        int ARR_SIZE = 100_000_000;
        arr1 = new int[ARR_SIZE];
        count = new int[DIAP];
        Random rnd = new Random();
        for (int i = 0; i < ARR_SIZE; i++) {
            arr1[i] = rnd.nextInt(DIAP) + 1;
        }
    }

    public String serialSort(){
        this.fillArray();
        long t1 = System.currentTimeMillis();
        sortAlgSerial(this.arr1);
        long t2 = System.currentTimeMillis();
        return "Time: " + (t2-t1) + "ms \n\n";
    }

    public int[] sortAlgSerial(int[] array) {
        int min, max;
        max = min = array[0];
        long t11 = System.currentTimeMillis();
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
            else if (array[i] > max) {
                max = array[i];
            }
        }
        System.out.println("Time finding min max:" + (System.currentTimeMillis() - t11));
        return this.sortAlgSerial(array, min, max);
    }

    int[] sortAlgSerial(int[] array, int min, int max) {
        for (int i = 0; i < array.length; i++) {
            count[array[i] - min]++;
        }
        int idx = 0;
        for (int i = 0; i < count.length; i++) {
            for (int j = 0; j < count[i]; j++) {
                array[idx++] = i + min;
            }
        }
        return array;
    }
}
