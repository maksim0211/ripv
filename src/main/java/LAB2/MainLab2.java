package LAB2;

import java.util.Arrays;
import java.util.Random;


public class MainLab2 {
    final static int VOLUME = 1_000_000;
    final static int DIAP = 10_000;


    public static int[] fillMassive(int capacity, int diap){
        Random rnd = new Random();
        int arr[] = new int[capacity];
        for (int i = 0; i < arr.length; ++i) {
            arr[i] = rnd.nextInt(diap) + 1;
        }
        return arr;
    }

    public static Boolean isEqual(int[] a, int[] b){
        if(a.length != b.length)
            return false;
        for(int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) throws Exception {
        int serial = 1;

        if (serial != 0) {
            int arr1[] = fillMassive(VOLUME, DIAP);
            int[] arr2 = Arrays.copyOf(arr1, VOLUME);
            int[] arr3 = Arrays.copyOf(arr1, VOLUME);
            int[] arr4 = Arrays.copyOf(arr1, VOLUME);
            int[] arr5 = Arrays.copyOf(arr1, VOLUME);
            int[] arr6 = Arrays.copyOf(arr1, VOLUME);
            int[] arr7 = Arrays.copyOf(arr1, VOLUME);

            //QuickSort
            long t1 = System.currentTimeMillis();
            Arrays.sort(arr1);
            System.out.println("QuickSort: " + (System.currentTimeMillis() - t1) + "ms\n");

            // each thread = each own variable
            System.out.println("Time parallel v1 : -----");
            int[] arrv1 = Lab2Parallel.sort(4, arr3, DIAP);
            System.out.println("------------------------");
            // using synchronized
            int[] arrv2 = Lab2Parallelv2.sort(4, arr4);
            //common variable (NO WORKING ITS NORMAL);
            int[] arrv3 = Lab2Parallelv3.sort(4, arr5);
            //using AtomicIntegerArray;
            int[] arrv4 = Lab2Parallelv4.sort(4, arr6);
            //using synchronized method;
            int[] arrv5 = Lab2Parallelv5.sort(4, arr7);

            System.out.println(isEqual(arrv1, arr1));
            System.out.println(isEqual(arrv2, arr1));
            System.out.println(isEqual(arrv3, arr1));
            System.out.println(isEqual(arrv4, arr1));
            System.out.println(isEqual(arrv5, arr1));
        }
        else
            System.out.println(new LAB2.Lab1Serial().serialSort());
    }
}
