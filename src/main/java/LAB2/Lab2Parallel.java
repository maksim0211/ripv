package LAB2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Lab2Parallel {
        static int diap;
        static int min = Integer.MAX_VALUE;
        static int max = Integer.MIN_VALUE;
        static int[] arr1;

    public static int[] sort (int n, int[] a, int diap) {
            final int[] cnt = new int[diap];
            arr1 = a;
        class MyThread extends Thread {
            int l;
            int r;
            int myMin = Integer.MAX_VALUE;
            int myMax = Integer.MIN_VALUE;
            long time;
            public final int[] mycnt = new int[diap];

            public MyThread(int l, int r) {
                this.l = l;
                this.r = r;
            }

            @Override
            public void run() {
                for (int i = l; i < r; i++) {
                    mycnt[arr1[i] - min]++;
                }
                for (int i = 0; i < diap; i++) {
                    synchronized (cnt) {
                        cnt[i] += mycnt[i];
                    }
                }
            }

            public void findMaxMin() {
                for (int i = l; i < r; i++) {
                    if (arr1[i] < myMin) {
                        myMin = arr1[i];
                    } else if (arr1[i] > myMax) {
                        myMax = arr1[i];
                    }
                }
            }
        }
        List<MyThread> list = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            if (i + 1 != n)
                list.add(new MyThread(arr1.length / n * i, arr1.length / n * (i + 1)));
            else
                //Чтобы не "потерять" последние элементы, если число элементов не делится нацело на число потоков
                list.add(new MyThread(arr1.length / n * i, arr1.length));
        }

        long t1 = System.currentTimeMillis();
        for (int i = 1; i < arr1.length; i++) {
            if (arr1[i] < min) {
                min = arr1[i];
            } else if (arr1[i] > max) {
                max = arr1[i];
            }
        }
        System.out.println("Time finding min and max:" + (t1 - System.currentTimeMillis()));

        long t3 = System.currentTimeMillis();
        list.forEach(Thread::start);
        list.forEach(x -> {
            try {
                x.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("Time of work multithread: " + (System.currentTimeMillis() - t3));

        long t5 = System.currentTimeMillis();
        int idx = 0;
        for (int i = 0; i < diap; i++) {
            Arrays.fill(arr1, idx, cnt[i] + idx, i + min);
            idx += cnt[i];
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Time fill arr: " + (t2 - t5) + "ms \t");
        System.out.println("Time parallel algo: " + (t2 - t1) + "ms \n\n");
        return arr1;
    }
}
