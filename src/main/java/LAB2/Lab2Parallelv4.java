package LAB2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicIntegerArray;


//Атомарная общая переменная
public class Lab2Parallelv4 {

    static AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(MainLab2.DIAP);

    public static int[] sort(int n, int[] arr1){
        long t1 = System.currentTimeMillis();
        int min, max;
        max = min = arr1[0];
        for (int i = 1; i < arr1.length; i++) {
            if (arr1[i] < min) {
                min = arr1[i];
            }
            else if (arr1[i] > max) {
                max = arr1[i];
            }
        }

        int finalMin1 = min;
        class MyThread extends Thread{
            int l;
            int r;
            long time;
            public MyThread(int l, int r){
                this.l = l;
                this.r = r;
            }
            @Override
            public void run(){
                for (int i = l; i < r; i++) {
                    atomicIntegerArray.incrementAndGet(arr1[i]-finalMin1);
                }
            }
        }

        List<MyThread> list = new ArrayList<>(n);
        for(int i = 0; i < n; i++)
            if (i + 1 != n)
                list.add(new MyThread(arr1.length / n * i, arr1.length / n * (i + 1)));
            else
                //Чтобы не "потерять" последние элементы, если число элементов не делится нацело на число потоков
                list.add(new MyThread(arr1.length / n * i, arr1.length));
        list.forEach(Thread::start);
        list.forEach(x -> {
            try {
                x.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        int idx = 0;
        // count[i] - показывает сколько раз встречается то или иное число
        for (int i = 0; i < atomicIntegerArray.length(); i++) {
            for (int j = 0; j < atomicIntegerArray.get(i); j++) {
                arr1[idx++] = i + min;
            }
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Time parallel v4: " + (t2-t1) + "ms \t");
        return arr1;
    }
}
