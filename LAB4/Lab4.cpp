#include <iostream>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <cstdlib>
#define n_elem 30000000
int a[n_elem];
int b[n_elem];

void quickSortSerial(int* c, long n) {
  long i = 0, j = n-1;
  int pivot = c[n / 2];
  do {
    while (c[i] < pivot) {
        i++;
    }
    while (c[j] > pivot) {
        j--;
    }
    if (i <= j) {
      std::swap(c[i], c[j]);
      i++; j--;
    }
  } while (i <= j);
  if (j > 0) {
      quickSortSerial(c, j+1);
  }
  if (n > i) {
      quickSortSerial(c + i, n - i);
  }
}

void quickSortParallel(int* d, long n){
  long i = 0, j = n-1;
  int pivot = d[n / 2];
  do {
    while (d[i] < pivot) {
        i++;
    }
    while (d[j] > pivot) {
        j--;
    }
    if (i <= j) {
      std::swap(d[i], d[j]);
      i++; j--;
    }
  } while (i <= j);
  
  //#pragma omp taskgroup
    {
    //50-75k оптимальное значение
    if (n/2 < 50000){
      #pragma omp task shared(d)
        if (j > 0) 
          quickSortSerial(d, j+1);
      #pragma omp task shared(d)
        if (n > i) 
          quickSortSerial(d + i, n - i);
    }
    else {
      #pragma omp task shared(d)
        if (j > 0)
          quickSortParallel(d, j+1);
      #pragma omp task shared(d)
        if (n > i) 
          quickSortParallel(d + i, n - i);
    }
    #pragma omp taskwait
  }
}

int main() {        
  for (int i = 0; i < n_elem; i++) {
      a[i] = rand() % 10000 + 1;
      b[i] = rand() % 10000 + 1;
  }
  double start_time = omp_get_wtime();
  omp_set_dynamic(0);
  omp_set_num_threads(2);
  #pragma omp parallel
  {
    #pragma omp single
    quickSortParallel(a,n_elem);
  }
  double end_time = omp_get_wtime();
  printf("Parallel algorithm time: %f s\n", end_time - start_time);

  for (int i = 0; i < n_elem - 1; i++) {
    if (a[i] > a[i+1]){
      printf("error: %d > %d at %d (i)\n", a[i], a[i+1], i);
    }
  }

  start_time = omp_get_wtime();
  quickSortSerial(b,n_elem);
  end_time = omp_get_wtime();
  printf("Serial algorithm time: %f s\n", end_time - start_time);

  for (int i = 0; i < n_elem - 1; i++) {
    if (b[i] > b[i+1]){
      printf("error: %d > %d at %d (i)\n", b[i], b[i+1], i);
    }
  }

  return 0;
}